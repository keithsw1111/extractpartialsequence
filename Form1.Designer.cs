﻿namespace ExtractPartialSequence
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnSelectShowDir = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtShowDir = new System.Windows.Forms.TextBox();
            this.txtOutputSequenceFile = new System.Windows.Forms.TextBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnCopyEndToStart = new System.Windows.Forms.Button();
            this.lblLength = new System.Windows.Forms.Label();
            this.btnSelectSequenceFile = new System.Windows.Forms.Button();
            this.txtSequenceFile = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnSelectOutputSequenceFile = new System.Windows.Forms.Button();
            this.txtStartTime = new System.Windows.Forms.MaskedTextBox();
            this.txtEndTime = new System.Windows.Forms.MaskedTextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Show Directory";
            // 
            // btnSelectShowDir
            // 
            this.btnSelectShowDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectShowDir.Location = new System.Drawing.Point(574, 13);
            this.btnSelectShowDir.Name = "btnSelectShowDir";
            this.btnSelectShowDir.Size = new System.Drawing.Size(33, 23);
            this.btnSelectShowDir.TabIndex = 2;
            this.btnSelectShowDir.Text = "...";
            this.btnSelectShowDir.UseVisualStyleBackColor = true;
            this.btnSelectShowDir.Click += new System.EventHandler(this.btnSelectShowDir_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Start Time";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "End Time";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Output Sequence File";
            // 
            // txtShowDir
            // 
            this.txtShowDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtShowDir.Location = new System.Drawing.Point(164, 14);
            this.txtShowDir.Name = "txtShowDir";
            this.txtShowDir.Size = new System.Drawing.Size(404, 22);
            this.txtShowDir.TabIndex = 1;
            this.txtShowDir.TextChanged += new System.EventHandler(this.txtShowDir_TextChanged);
            // 
            // txtOutputSequenceFile
            // 
            this.txtOutputSequenceFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutputSequenceFile.Location = new System.Drawing.Point(164, 128);
            this.txtOutputSequenceFile.Name = "txtOutputSequenceFile";
            this.txtOutputSequenceFile.Size = new System.Drawing.Size(404, 22);
            this.txtOutputSequenceFile.TabIndex = 7;
            this.txtOutputSequenceFile.TextChanged += new System.EventHandler(this.txtOutputSequenceFile_TextChanged);
            // 
            // btnCreate
            // 
            this.btnCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreate.Location = new System.Drawing.Point(532, 156);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 9;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnCopyEndToStart
            // 
            this.btnCopyEndToStart.Location = new System.Drawing.Point(270, 100);
            this.btnCopyEndToStart.Name = "btnCopyEndToStart";
            this.btnCopyEndToStart.Size = new System.Drawing.Size(216, 23);
            this.btnCopyEndToStart.TabIndex = 10;
            this.btnCopyEndToStart.Text = "Copy End Time To Start Time";
            this.btnCopyEndToStart.UseVisualStyleBackColor = true;
            this.btnCopyEndToStart.Click += new System.EventHandler(this.btnCopyEndToStart_Click);
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(270, 77);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(184, 17);
            this.lblLength.TabIndex = 11;
            this.lblLength.Text = "Sequence Length: 00:00.00";
            // 
            // btnSelectSequenceFile
            // 
            this.btnSelectSequenceFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectSequenceFile.Location = new System.Drawing.Point(574, 41);
            this.btnSelectSequenceFile.Name = "btnSelectSequenceFile";
            this.btnSelectSequenceFile.Size = new System.Drawing.Size(33, 23);
            this.btnSelectSequenceFile.TabIndex = 4;
            this.btnSelectSequenceFile.Text = "...";
            this.btnSelectSequenceFile.UseVisualStyleBackColor = true;
            this.btnSelectSequenceFile.Click += new System.EventHandler(this.btnSelectSequenceFile_Click);
            // 
            // txtSequenceFile
            // 
            this.txtSequenceFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSequenceFile.Location = new System.Drawing.Point(164, 42);
            this.txtSequenceFile.Name = "txtSequenceFile";
            this.txtSequenceFile.Size = new System.Drawing.Size(404, 22);
            this.txtSequenceFile.TabIndex = 3;
            this.txtSequenceFile.TextChanged += new System.EventHandler(this.txtSequenceFile_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "Sequence File";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "xml";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Sequence Files (*.xml, *.xsq)|*.xml;*.xsq";
            // 
            // btnSelectOutputSequenceFile
            // 
            this.btnSelectOutputSequenceFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectOutputSequenceFile.Location = new System.Drawing.Point(574, 125);
            this.btnSelectOutputSequenceFile.Name = "btnSelectOutputSequenceFile";
            this.btnSelectOutputSequenceFile.Size = new System.Drawing.Size(33, 23);
            this.btnSelectOutputSequenceFile.TabIndex = 8;
            this.btnSelectOutputSequenceFile.Text = "...";
            this.btnSelectOutputSequenceFile.UseVisualStyleBackColor = true;
            this.btnSelectOutputSequenceFile.Click += new System.EventHandler(this.btnSelectOutputSequenceFile_Click);
            // 
            // txtStartTime
            // 
            this.txtStartTime.Location = new System.Drawing.Point(164, 69);
            this.txtStartTime.Mask = "00:00.000";
            this.txtStartTime.Name = "txtStartTime";
            this.txtStartTime.Size = new System.Drawing.Size(100, 22);
            this.txtStartTime.TabIndex = 5;
            this.txtStartTime.Text = "0000000";
            this.txtStartTime.TextChanged += new System.EventHandler(this.txtStartTime_TextChanged_1);
            // 
            // txtEndTime
            // 
            this.txtEndTime.Location = new System.Drawing.Point(164, 97);
            this.txtEndTime.Mask = "00:00.000";
            this.txtEndTime.Name = "txtEndTime";
            this.txtEndTime.Size = new System.Drawing.Size(100, 22);
            this.txtEndTime.TabIndex = 6;
            this.txtEndTime.Text = "0000000";
            this.txtEndTime.TextChanged += new System.EventHandler(this.txtEndTime_TextChanged_1);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "xml";
            this.saveFileDialog1.Filter = "Sequence Files (*.xsq)|*.xsq";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 186);
            this.Controls.Add(this.txtEndTime);
            this.Controls.Add(this.txtStartTime);
            this.Controls.Add(this.btnSelectOutputSequenceFile);
            this.Controls.Add(this.btnSelectSequenceFile);
            this.Controls.Add(this.txtSequenceFile);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblLength);
            this.Controls.Add(this.btnCopyEndToStart);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.txtOutputSequenceFile);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSelectShowDir);
            this.Controls.Add(this.txtShowDir);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "xLights Sequence Slicer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSelectShowDir;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtShowDir;
        private System.Windows.Forms.TextBox txtOutputSequenceFile;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Button btnCopyEndToStart;
        private System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.Button btnSelectSequenceFile;
        private System.Windows.Forms.TextBox txtSequenceFile;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnSelectOutputSequenceFile;
        private System.Windows.Forms.MaskedTextBox txtStartTime;
        private System.Windows.Forms.MaskedTextBox txtEndTime;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

