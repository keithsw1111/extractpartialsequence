﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;

namespace ExtractPartialSequence
{
    public partial class Form1 : Form
    {
        TimeSpan tsSeqLength = TimeSpan.Zero;
        private int framesize = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void txtShowDir_TextChanged(object sender, EventArgs e)
        {
            ValidateWindow();
        }

        private void txtSequenceFile_TextChanged(object sender, EventArgs e)
        {
            tsSeqLength = GetSequenceLength(txtSequenceFile.Text);
            ValidateWindow();
        }

        private void txtOutputSequenceFile_TextChanged(object sender, EventArgs e)
        {
            ValidateWindow();
        }

        private void btnSelectShowDir_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog(this) == DialogResult.OK)
            {
                txtShowDir.Text = folderBrowserDialog1.SelectedPath;
                ValidateWindow();
            }
        }

        private void btnSelectSequenceFile_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(txtShowDir.Text))
            {
                openFileDialog1.InitialDirectory = txtShowDir.Text;
            }

            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                txtSequenceFile.Text = openFileDialog1.FileName;
                ValidateWindow();
                tsSeqLength = GetSequenceLength(txtSequenceFile.Text);
                lblLength.Text = "Sequence Length: " + tsSeqLength.ToString(@"hh\:mm\:ss\.fff");
            }

        }

        private TimeSpan GetSequenceLength(string file)
        {
            TimeSpan ts = TimeSpan.Zero;
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.Load(file);

                XmlNodeList xnList = xml.SelectNodes("/xsequence/head/sequenceDuration");

                if (xnList.Count == 1)
                {
                    double secs = double.Parse(xnList[0].InnerText);
                    ts = TimeSpan.FromSeconds(secs);
                }

                xnList = xml.SelectNodes("/xsequence/head/sequenceTiming");
                if (xnList.Count == 1)
                {
                    string ms = xnList[0].InnerText;
                    ms = ms.Replace(" ", "");
                    ms = ms.Replace("ms", "");
                    framesize = int.Parse(ms);
                }
            }
            catch (Exception)
            {
            }

            return ts;
        }

        private void btnSelectOutputSequenceFile_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(txtShowDir.Text))
            {
                saveFileDialog1.InitialDirectory = txtShowDir.Text;
            }

            if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                txtOutputSequenceFile.Text = saveFileDialog1.FileName;
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            TimeSpan tsStart = TimeSpan.ParseExact(txtStartTime.Text, @"mm\:ss\.fff", CultureInfo.InvariantCulture);
            TimeSpan tsEnd = TimeSpan.ParseExact(txtEndTime.Text, @"mm\:ss\.fff", CultureInfo.InvariantCulture);
            int startms = (int)tsStart.TotalMilliseconds;
            int endms = (int)tsEnd.TotalMilliseconds;
            int duration = endms - startms;

            XmlDocument xmlin = new XmlDocument();
            xmlin.Load(txtSequenceFile.Text);
            XPathNavigator nav = xmlin.CreateNavigator();
            nav.MoveToRoot();
            nav.MoveToChild("xsequence", "");

            StreamWriter sw = new StreamWriter(txtOutputSequenceFile.Text);
            sw.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            sw.WriteLine("<xsequence BaseChannel=\""+nav.GetAttribute("BaseChannel", "")+ 
                                 "\" ChanCtrlBasic=\""+ nav.GetAttribute("ChanCtrlBasic", "") + 
                                 "\" ChanCtrlColor=\""+ nav.GetAttribute("ChanCtrlColor", "") + 
                                 "\" FixedPointTiming=\""+ nav.GetAttribute("FixedPointTiming", "") + 
                                 "\">");

            bool cont = nav.MoveToChild(XPathNodeType.Element);

            while (cont)
            {
                if (nav.Name == "head")
                {
                    sw.WriteLine("  <head>");
                    bool h = nav.MoveToChild(XPathNodeType.Element);

                    while (h)
                    {
                        if (nav.Name == "sequenceDuration")
                        {
                            sw.WriteLine("    <sequenceDuration>"+(duration/1000).ToString()+"."+(duration%1000).ToString("000")+"</sequenceDuration>");
                        }
                        else
                        {
                            sw.WriteLine(nav.OuterXml);
                        }
                        h=nav.MoveToNext();
                    }

                    nav.MoveToParent();
                    sw.WriteLine("  </head>");
                }
                else if (nav.Name == "ElementEffects")
                {
                    sw.WriteLine("  <ElementEffects>");

                    bool ee = nav.MoveToChild(XPathNodeType.Element);

                    while (ee)
                    {
                        sw.WriteLine("    <Element type=\"" + nav.GetAttribute("type", "") +
                                             "\" name=\"" + nav.GetAttribute("name", "") +
                                             "\">");

                        bool el = nav.MoveToChild(XPathNodeType.Element);

                        while (el)
                        {
                            if (!nav.HasChildren)
                            {
                                sw.WriteLine("      <EffectLayer/>");
                            }
                            else
                            {
                                sw.WriteLine("      <EffectLayer>");
                                bool ef = nav.MoveToChild(XPathNodeType.Element);

                                while (ef)
                                {
                                    try
                                    {
                                        string sts = nav.GetAttribute("startTime", "");
                                        string ens = nav.GetAttribute("endTime", "");
                                        int st = int.Parse(nav.GetAttribute("startTime", ""));
                                        int en = int.Parse(nav.GetAttribute("endTime", ""));

                                        if (en <= startms || st >= endms)
                                        {
                                            // we can exclude these
                                        }
                                        else
                                        {
                                            // adjust early effects
                                            if (st < startms)
                                            {
                                                st = startms;
                                            }
                                            // adjust late effects
                                            if (en > endms)
                                            {
                                                en = endms;
                                            }
                                            sw.WriteLine("        <Effect ref=\"" + nav.GetAttribute("ref", "") +
                                                         "\" label=\"" + nav.GetAttribute("label", "") +
                                                         "\" name=\"" + nav.GetAttribute("name", "") +
                                                         "\" selected=\"" + nav.GetAttribute("selected", "") +
                                                         "\" startTime=\"" + (st - startms).ToString() +
                                                         "\" endTime=\"" + (en - startms).ToString() +
                                                         "\" palette=\"" + nav.GetAttribute("palette", "") +
                                                         "\"/>");
                                        }
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    ef = nav.MoveToNext();
                                }

                                sw.WriteLine("      </EffectLayer>");
                                nav.MoveToParent();
                            }

                            el = nav.MoveToNext();
                        }
                        nav.MoveToParent();

                        sw.WriteLine("    </Element>");
                        ee = nav.MoveToNext();
                    }

                    sw.WriteLine("  </ElementEffects>");

                    nav.MoveToParent();
                }
                else
                {
                    sw.WriteLine(nav.OuterXml);
                }
                cont = nav.MoveToNext();
            }

            sw.WriteLine("</xsequence>");
            sw.Close();

            MessageBox.Show(@"Done.");
        }

        private void ValidateWindow()
        {
            bool ok = true;

            if (!Directory.Exists(txtShowDir.Text) || !File.Exists(txtShowDir.Text + "\\xlights_rgbeffects.xml"))
            {
                ok = false;
            }

            if (!File.Exists(txtSequenceFile.Text))
            {
                ok = false;
            }

            try
            {
                string p = Path.GetDirectoryName(txtOutputSequenceFile.Text);
                if (p != "" && !Directory.Exists(p))
                {
                    ok = false;
                }
            }
            catch (Exception)
            {
                ok = false;
            }

            if (ok)
            {
                TimeSpan tsStart = TimeSpan.Zero;
                try
                {
                    tsStart = TimeSpan.ParseExact(txtStartTime.Text, @"mm\:ss\.fff", CultureInfo.InvariantCulture);
                    if (tsStart > tsSeqLength)
                    {
                        ok = false;
                    }
                }
                catch (Exception ex)
                {
                    ok = false;
                }
                TimeSpan tsEnd = TimeSpan.Zero;
                try
                {
                    tsEnd = TimeSpan.ParseExact(txtEndTime.Text, @"mm\:ss\.fff", CultureInfo.InvariantCulture);
                    if (tsEnd > tsSeqLength)
                    {
                        ok = false;
                    }
                }
                catch (Exception)
                {
                    ok = false;
                }

                if (ok)
                {
                    if (tsStart >= tsEnd)
                    {
                        ok = false;
                    }
                }
            }

            if (ok)
            {
                btnCreate.Enabled = true;
            }
            else
            {
                btnCreate.Enabled = false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ValidateWindow();
        }

        private void btnCopyEndToStart_Click(object sender, EventArgs e)
        {
            txtStartTime.Text = txtEndTime.Text;
            ValidateWindow();
        }

        private void txtStartTime_TextChanged_1(object sender, EventArgs e)
        {
            ValidateWindow();
        }

        private void txtEndTime_TextChanged_1(object sender, EventArgs e)
        {
            ValidateWindow();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }
    }
}
